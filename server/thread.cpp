#include "thread.h"
#include <QJsonDocument>
#include <QJsonObject>
#include "server.h"

QList<QTcpSocket *> Server::connections;
QJsonObject Server::nicks;
QJsonObject Server::nicksWithDes;


Thread::Thread(qintptr ID, QObject *parent) :
    QThread(parent)
{
    this->socketDescriptor = ID;
}
void Thread::run()
{
    qDebug() << " Thread started";

    socket = new QTcpSocket();


    if(!socket->setSocketDescriptor(this->socketDescriptor))
    {
        emit error(socket->error());
        return;
    }
    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()), Qt::DirectConnection);
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));

    qDebug() << socketDescriptor << " Client connected";
    Server::connections.append(socket);

    exec();
}

void Thread::readyRead()
{
    QByteArray Data = socket->readAll();
    QJsonDocument jsondoc = QJsonDocument::fromJson(Data);
    QJsonObject json = jsondoc.object();
    if(json.value("type") == 1) {
        QString output = QString("%1 %2: %3 ")
                .arg(json.value("time").toString()).arg(json.value("nick").toString()).arg(json.value("message").toString());
        emit send(Data);
        qDebug() << socketDescriptor << " Message: " << output;

    }
    else if (json.value("type") == 2) {
        //connected
        qDebug() << socketDescriptor << " Connected: " << json.value("nick").toString();
        double size = Server::nicks.size() +1;
        Server::nicks.insert(QString::number(size), json.value("nick").toString());
        Server::nicks.insert("type", 2);
        Server::nicksWithDes.insert(QString::number(socketDescriptor), json.value("nick").toString());
        QJsonDocument jdoc = QJsonDocument(Server::nicks);
        emit send(jdoc.toJson());
        Server::nicks.remove("type");
    }
    else if (json.value("type") == 3)
    {
        //disconnected
        qDebug() << socketDescriptor << " Disconnected: " << json.value("nick");

    }

}


void Thread::disconnected()
{

    qDebug() << socketDescriptor << " Disconnected";
    QString nick = Server::nicksWithDes[QString::number(socketDescriptor)].toString();
    Server::nicksWithDes.remove(QString::number(socketDescriptor));
    for(int i=1; i<=Server::nicks.size(); i++)
        {

            if(Server::nicks[QString::number(i)] == nick && Server::nicks[QString::number(i)] != "")
            {
                Server::nicks.remove(QString::number(i));
            }

        }
    Server::nicks.insert("type", 2);
    QJsonDocument jdoc = QJsonDocument(Server::nicks);
    emit send(jdoc.toJson());
    Server::nicks.remove("type");

    for(int j=1; j<Server::connections.size(); j++)
    {

        if(Server::connections[j] == socket) {
            Server::connections.removeAt(j);
        }
    }

    socket->deleteLater();
    exit(0);
}

