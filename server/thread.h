#ifndef THREAD_H
#define THREAD_H

#include <QObject>
#include <QThread>
#include <QTcpSocket>
#include <QDebug>
#include <QList>

class Thread : public QThread
{
    Q_OBJECT
public:
    explicit Thread(qintptr ID, QObject *parent = 0);

    void run();

signals:
    void error(QTcpSocket::SocketError socketerror);
    void send(QByteArray data);


public slots:
    void readyRead();
    void disconnected();


private:
    QTcpSocket *socket;
    qintptr socketDescriptor;
};

#endif // THREAD_H
