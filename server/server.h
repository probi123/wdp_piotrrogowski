#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QJsonDocument>
#include "thread.h"

class Server : public QTcpServer
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = 0);
    void startServer();
    static QList<QTcpSocket *> connections;
    static QJsonObject nicks;
    static QJsonObject nicksWithDes;


signals:
    void send(QByteArray data);
public slots:
    void sendToAllConnections(QByteArray data);
protected:
    void incomingConnection(qintptr socketDescriptor);
private:
    QTcpSocket socket;
};

#endif // SERVER_H
