QT += core
QT += network
QT -= gui

CONFIG += c++11

TARGET = server
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    server.cpp \
    thread.cpp

HEADERS += \
    server.h \
    thread.h
