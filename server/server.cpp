#include "server.h"
#include "thread.h"
Server::Server(QObject *parent) :
    QTcpServer(parent)
{
}

void Server::startServer()
{
    int port = 4444;

    if(!this->listen(QHostAddress::Any,port))
    {
        qDebug() << "Could not start server";
    }
    else
    {
        qDebug() << "Listening to port " << port << "...";
    }
}

void Server::incomingConnection(qintptr socketDescriptor)
{
    qDebug() << socketDescriptor << " Connecting...";

    Thread *thread = new Thread(socketDescriptor, this);

    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(thread, SIGNAL(send(QByteArray)),this,
            SLOT(sendToAllConnections(QByteArray)), Qt::QueuedConnection );
    thread->start();
}

void Server::sendToAllConnections(QByteArray data)
{
        foreach(QTcpSocket *connection, Server::connections) {
            if(!Server::connections.isEmpty()){
                connection->write(data);
            }
        }
}


