#ifndef EMOTICONS_H
#define EMOTICONS_H

#include <QWidget>

namespace Ui {
class emoticons;
}

class emoticons : public QWidget
{
    Q_OBJECT

public:
    explicit emoticons(QWidget *parent = 0);
    ~emoticons();
public slots:
    void show();

private:
    Ui::emoticons *ui;
};

#endif // EMOTICONS_H
