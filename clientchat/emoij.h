#ifndef EMOIJ_H
#define EMOIJ_H

#include <QDialog>

namespace Ui {
class Emoij;
}

class Emoij : public QDialog
{
    Q_OBJECT

public:
    explicit Emoij(QWidget *parent = 0);
    ~Emoij();
signals:
    void sendEmoij(QString);
private slots:

    void on_but1_clicked();
    void on_but2_clicked();
    void on_but3_clicked();
    void on_but4_clicked();
    void on_but5_clicked();
    void on_but6_clicked();
    void on_but7_clicked();
    void on_but8_clicked();
    void on_but9_clicked();
    void on_but10_clicked();
    void on_but11_clicked();
    void on_but12_clicked();
    void on_but13_clicked();
    void on_but14_clicked();
    void on_but15_clicked();



private:
    Ui::Emoij *ui;
};

#endif // EMOIJ_H
