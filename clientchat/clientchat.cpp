#include "clientchat.h"
#include "ui_clientchat.h"
#include "emoij.h"
#include <QtNetwork>
#include <QJsonArray>
#include <QJsonDocument>
#include <QTime>
#include <QFontDatabase>
#include <QFont>
#include <QDialog>
#include <QKeyEvent>
#include <QEvent>
#include <QObject>
#include <QTextEdit>
#include <QWidget>

ClientChat::ClientChat(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ClientChat)
{
    ui->setupUi(this);

}

ClientChat::~ClientChat()
{
    delete ui;
}
bool ClientChat::start(QString address, quint16 port)
{
  QHostAddress addr(address);
  client.connectToHost(addr, port);
  MyQTedit *filter = new MyQTedit(this);
     ui->message->installEventFilter(filter);
     connect(filter, SIGNAL(entered()), SLOT(on_sendButton_clicked()));
    colors << "#FF00FF" << "#800080" << "#00FFFF" << "#008000" << "#808000" << "#800000" << "#FF0000" << "#008080" << "#FFC300" << "#999999";

}

void ClientChat::sendMessage(QJsonDocument json)
{
           if(!json.isEmpty())
           {
               client.write(json.toJson());
           }
}


void ClientChat::readyRead()
    {

    QByteArray data = client.readAll();
    QJsonDocument jsondoc = QJsonDocument::fromJson(data);
    QJsonObject json = jsondoc.object();
    if(json.value("type") == 1 && json.value("message") != "") {
        QString output = QString("<i>%1</i> <b><font color ='%2'> %3</font></b>: %4 ")
                .arg(json.value("time").toString()).arg(nicks[json.value("nick").toString()].toString()).arg(json.value("nick").toString()).arg(json.value("message").toString());
        ui->chatBox->append(output);
    }
    else if(json.value("type") == 2) {
        //conneted
        ui->nicksBox->setText("");
        int size = json.size();

        for(int i=1; i<=size; i++)
        {
             ui->nicksBox->append("<font color='"+colors[i]+"'>"+json.value(QString::number(i)).toString()+"</font>");
             nicks.insert(json.value(QString::number(i)).toString(), colors[i] );

        }
    }

    }



void ClientChat::on_connect_clicked()
{
    if(ui->connect->text() == "Connect")
    {
        QString addr = ui->addr->text();
        quint16 port = ui->port->text().toInt();
        ui->addr->setReadOnly(true);
        ui->port->setReadOnly(true);
        ui->nick->setReadOnly(true);
        this->start(addr, port);
        connect(&client, SIGNAL(readyRead()),this, SLOT(readyRead()));
        //send information about client
        QJsonObject json;
        json["type"]= 2;
        json["nick"] = ui->nick->text();
        QJsonDocument jsondoc;
        jsondoc = QJsonDocument(json);
        sendMessage(jsondoc);
        ui->connect->setText("Disconnect");
        ui->chatBox->append("Connected...");
    }
    else if(ui->connect->text() == "Disconnect")
    {
        client.abort();
        ui->nicksBox->setText("");
        ui->connect->setText("Connect");
        ui->chatBox->append("Disconnected...");
        ui->addr->setReadOnly(false);
        ui->port->setReadOnly(false);
        ui->nick->setReadOnly(false);

    }


}


void ClientChat::on_sendButton_clicked()
{
    QJsonObject json;
    json["type"]= 1;
    json["message"] = ui->message->toPlainText();
    json["nick"] = ui->nick->text();
    json["time"] = QTime::currentTime().toString();
    QJsonDocument jsondoc;
    ui->message->setText("");
    jsondoc = QJsonDocument(json);
    sendMessage(jsondoc);
}

void ClientChat::on_toolButton_clicked()
{
    Emoij emo;
    connect(&emo, SIGNAL(sendEmoij(QString)), SLOT(addEmoij(QString)));

    emo.setModal(true);
    emo.exec();

}
void ClientChat::addEmoij(QString emo)
{
    ui->message->moveCursor(QTextCursor::End);
    ui->message->insertPlainText(emo);


}


