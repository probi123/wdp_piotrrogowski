#ifndef MYQTEDIT_H
#define MYQTEDIT_H
#include <QTextEdit>
#include <QObject>
#include "clientchat.h"


class MyQTedit : public QObject
{
    Q_OBJECT
public:
    explicit MyQTedit(QObject *parent = 0);

signals:
    void entered();
protected:
  bool eventFilter(QObject *obj, QEvent *event);
public slots:
private:
};



#endif // MYQTEDIT_H
