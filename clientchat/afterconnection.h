#ifndef AFTERCONNECTION_H
#define AFTERCONNECTION_H

#include <QDialog>

namespace Ui {
class AfterConnection;
}

class AfterConnection : public QDialog
{
    Q_OBJECT

public:
    explicit AfterConnection(QWidget *parent = 0);
    ~AfterConnection();

private:
    Ui::AfterConnection *ui;
};

#endif // AFTERCONNECTION_H
