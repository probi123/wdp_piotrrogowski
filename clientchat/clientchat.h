#ifndef CLIENTCHAT_H
#define CLIENTCHAT_H
#include "myqtedit.h"
#include <QMainWindow>
#include <QtNetwork>
#include <QObject>
#include <QString>
#include <QTcpSocket>
#include <QList>

namespace Ui {
class ClientChat;
}

class ClientChat : public QMainWindow
{
    Q_OBJECT

public:
    explicit ClientChat(QWidget *parent = 0);
    ~ClientChat();
    bool start(QString address, quint16 port);
signals:

protected:

public slots:
  void readyRead();
  void sendMessage(QJsonDocument json);
  void addEmoij(QString emo);
  void on_sendButton_clicked();


private slots:
  void on_connect_clicked();


  void on_toolButton_clicked();


private:
    QTcpSocket client;
    Ui::ClientChat *ui;
    QVector<QString> colors;
    QJsonObject nicks;

};

#endif // CLIENTCHAT_H
