#include "clientchat.h"
#include <QApplication>
#include <QFontDatabase>
#include "ui_clientchat.h"
#include "myqtedit.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ClientChat w;
    w.show();
    QFontDatabase fontDB;
        fontDB.addApplicationFont(":/resources/fonts/segoeuisymbol.ttf");
        QApplication::setFont(QFont(QStringLiteral("Segoe UI Symbol")));

    return a.exec();
}
