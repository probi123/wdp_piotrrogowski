#-------------------------------------------------
#
# Project created by QtCreator 2016-11-30T11:58:20
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = clientchat
TEMPLATE = app


SOURCES += main.cpp\
        clientchat.cpp \
    emoij.cpp \
    myqtedit.cpp

HEADERS  += clientchat.h \
    emoij.h \
    myqtedit.h

FORMS    += clientchat.ui \
    emoij.ui
